# Web Perf Workshop

*by [Inian Parameshwaran](https://twitter.com/everConfusedGuy) and [Sebastiaan Deckers](https://twitter.com/sebdeckers) at [JSConf.Asia 2018](https://2018.jsconf.asia/)*

## Format
- Half day duration (3.5 hours)
- Classroom style venue layout
- BYO laptop
- Switch between lecture, demo, code-along, Q&A

## Goals
Both informational as well as applied:
- Participants will learn about upcoming standards, formats, protocols, technologies, and tools.
- Understand their impact on old and new patterns used to optimise web performance.
- Analyse, measure, and optimise common performance problems using new approaches.

## Content Outline
- Intro on HTTP/2
  - Protocol concepts
  - Deprecating on old habits?
  - Opportunity for new web perf patterns
  - Demo
- Upcoming standards related to web perf
  - IETF (HTTP-WG)
  - W3C/WHATWG (Fetch, SW)
  - ECMA (ESnext proposals)
  - IEEE (5G, 802.11)
- Tooling: new approaches to old problems
  - ES Modules vs Webpack/Babel
  - Inline/concat vs push
  - Local development/testing/CI/CD
  - Production deployment
- Metrics & Benchmarking
